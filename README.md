# classroombookings - sistema de reserva de salas de código aberto para escolas.

Adptado por Marcos Eduaro de Souza.

[![Licença: AGPLv3](https://img.shields.io/static/v1?label=License&message=AGPLv3&color=3DA639&style=flat-square)](https://www.gnu.org/licenses/agpl-3.0.html)
[![Twitter Follow](https://img.shields.io/twitter/follow/crbsapp.svg?style=social)](https://twitter.com/crbsapp)

Este é um sistema de agendamentos de salas baseado no open sorce (https://www.classroombookings.com/), desenvolvido para escolas e foi projetado para ser o mais fácil possível de usar. Podendo configurar quais salas podem ser agendadas, seus horários e o calendario escolar. Adicione contas de usuário e permita que eles façam e gerenciem agendamentos de qualquer lugar.

Ele está disponível para [baixar] em (https://www.classroombookings.com/download/)

É baseado na web - PHP e MySQL - e atualmente usa o framework [CodeIgniter 3](https://codeigniter.com/).

## Documentação
Para obter instruções de instalação e guia de configuração, [leia as páginas de documentação](https://www.classroombookings.com/documentation/).

## Relatórios de bug e solicitações de recursos
Verifique [GitHub Issues](https://github.com/craigrodway/classroombookings/issues) para ver os problemas existentes ou abrir um novo relatório de bug.

## Segurança
Para relatar quaisquer problemas de segurança, envie um e-mail para marcos.eduardo22@gmail.com em vez de usar o rastreador de problemas.